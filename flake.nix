{
    inputs.nixpkgs.url = "nixpkgs";

    outputs = { self, nixpkgs }: {
        packages.x86_64-darwin.default = let
            pkgs = import nixpkgs { system = "x86_64-darwin"; };
        in derivation {
            name = "adaedra-blog";
            system = "x86_64-darwin";
            builder = "${pkgs.bash}/bin/bash";
            args = [ ./build.sh ];

            PATH = with pkgs; lib.strings.makeBinPath [ coreutils ];
            src = ./.;
        };

        devShells.x86_64-darwin.default = with import nixpkgs { system = "x86_64-darwin"; };
        mkShell {
            buildInputs = [ nodejs-16_x ];
        };
    };
}
